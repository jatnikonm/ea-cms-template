Currently this project has 3 live instances:
- 1fortheworld.org chapter's resource - http://chapters.1fortheworld.org/
- it's the official demo for [DjangoCMS project](https://github.com/divio/django-cms/) - https://control.divio.com/demo/get-new/
- Effective Altruism Netherlands - https://effectiefaltruisme.nl/en/


Development Setup
-------------------------------------------------------------------------------
Built on Python 3.6, Django 2.2, DjangoCMS 3.7, Webpack 4, TypeScript 3.

See the general [setup instructions](https://gitlab.com/what-digital/djangocms-template/-/blob/master/docs/setup-instruction.md)

[Project intro & guidelines](https://gitlab.com/what-digital/djangocms-template/-/blob/master/docs/README.md)


Codebase Source
-------------------------------------------------------------------------------

This project is built upon [djangocms-template](https://gitlab.com/what-digital/djangocms-template).

The fixes & features from that project are being regularly merged into this repository.
