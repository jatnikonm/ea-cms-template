from cms.models import CMSPlugin
from djangocms_text_ckeditor.fields import HTMLField
from djangocms_text_ckeditor.models import AbstractText


class ReferenceTooltipPluginModel(AbstractText):
    pass
